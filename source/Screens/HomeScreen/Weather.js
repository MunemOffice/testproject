
  
  import React, {useEffect, useState} from 'react';
  import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    FlatList,
    Image,
    TouchableOpacity,
    Modal,
    ScrollView
  } from 'react-native';
//   import DropDownPicker from 'react-native-dropdown-picker';
  import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart,
  } from 'react-native-chart-kit';
  
  const {height, width} = Dimensions.get('window');
  const WEATHER_URI =
    'http://api.openweathermap.org/data/2.5/forecast?q=Karachi,74000&appid=1ff67b9e3ee07e15bb6e91da2bbca2a7';
  const primaryDarkTextColor = '#03B6CA';
  const primaryLightTextColor = '#00DCF4';
  const WEATHER_ICON =
    'https://www.freeiconspng.com/uploads/weather-icon-png-25.png';
  
  const WeatherApp = () => {
    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
  
    const [ViewMap, setViewMap] = useState(false);
  
    const [items, setItems] = useState([
      {label: 'Rawalpindi', value: 'Rawalpindi'},
      {label: 'Lahore', value: 'Lahore'},
      {label: 'Karachi', value: 'Karachi'},
      {label: 'Islamabad', value: 'Islamabad'},
    ]);
  
    const data = {
      labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      datasets: [
        {
          data: [20, 45, 28, 80, 99, 43],
          color: () => `${primaryDarkTextColor}`, // optional
          strokeWidth: 2, // optional
        },
      ],
      legend: ['Temprature'], // optional
    };
  
    const chartConfig = {
      backgroundGradientFrom: '#fff',
      backgroundGradientFromOpacity: 0,
      backgroundGradientTo: '#fff',
      backgroundGradientToOpacity: 0.5,
      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
      strokeWidth: 2, // optional, default 3
      barPercentage: 0.5,
      useShadowColorFromDataset: false, // optional
    };
  
    useEffect(() => {
      fetch(WEATHER_URI, {
        //   method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
        .then(res => res.json())
        .then(json_res => {
          console.log(json_res);
        })
        .catch(error => {
          console.log('error : ', error);
        })
        .finally(() => {
          console.log('called');
        });
    }),
      [];
  
    const renderWeatherCards = (item, index) => {
      return (
        <View style={styles.card}>
          <Image
            style={{height: 50, width: 50, resizeMode: 'contain'}}
            source={{uri: WEATHER_ICON}}
          />
          <Text
            style={{
              color: '#fff',
              fontSize: 26,
              letterSpacing: 1,
              fontWeight: '600',
            }}>
            {item.temprature}
          </Text>
          <Text style={{color: '#000'}}>{item.description}</Text>
          <Text style={{color: '#000'}}>{item.date}</Text>
        </View>
      );
    };
    return (
      <View style={styles.contianer}>
        <View style={styles.topContainer}>
          <Text style={styles.headerText}>Weather Listing Heading</Text>
          <View style={styles.dropdownContainer}>
            <DropDownPicker
              open={open}
              value={value}
              items={items}
              setOpen={setOpen}
              setValue={setValue}
              setItems={setItems}
            />
          </View>
        </View>
        <View style={styles.middleContainer}>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            style={{flexGrow: 0}}
            data={[...Array(5).keys()].map(val => {
              return {
                temprature: val + 'C',
                description: val + ' decription',
                date: new Date().toLocaleDateString(),
                image: null,
              };
            })}
            renderItem={({item, index}) => renderWeatherCards(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
  
          <ScrollView horizontal>
            <View style={{marginHorizontal: 16, alignItems: 'center', flex: 1}}>
              <LineChart
                style={{backgroundColor: '#fff'}}
                data={data}
                width={width}
                height={220}
                chartConfig={chartConfig}
              />
            </View>
          </ScrollView>
        </View>
        <View style={styles.bottomContainer}>
          <TouchableOpacity
            onPress={() => setViewMap(!ViewMap)}
            style={styles.viewButton}>
            <Text style={styles.buttonText}>
              View {!value ? 'City' : value} on Map
            </Text>
          </TouchableOpacity>
        </View>
  
        <Modal
          animationType="slide"
          visible={ViewMap}
          onRequestClose={() => setViewMap(!ViewMap)}>
          <ViewCityOnMap />
        </Modal>
      </View>
    );
  };
  
  const ViewCityOnMap = () => {
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <Text>mofal</Text>
      </View>
    );
  };
  
  const styles = StyleSheet.create({
    contianer: {
      flex: 1,
      backgroundColor: '#fff',
    },
    topContainer: {
      flex: 0.2,
      justifyContent: 'center',
      alignItems: 'center',
    },
    middleContainer: {
      flex: 0.7,
    },
    bottomContainer: {
      flex: 0.1,
      alignItems: 'flex-end',
      paddingHorizontal: 16,
      justifyContent: 'center',
    },
    headerText: {
      fontSize: 22,
      color: '#000',
      marginVertical: 16,
      fontWeight: '700',
    },
    dropdownContainer: {
      flex: 1,
      backgroundColor: '#fff',
      width: '50%',
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      zIndex: 100,
    },
    card: {
      borderRadius: 8,
      backgroundColor: primaryDarkTextColor,
      width: 120,
      height: 150,
      margin: 16,
      justifyContent: 'center',
      alignItems: 'center',
      elevation: 10,
    },
    viewButton: {
      backgroundColor: primaryDarkTextColor,
      borderRadius: 6,
      padding: 12,
      // width: '50%',
      justifyContent: 'center',
      alignItems: 'center',
      elevation: 5,
    },
    buttonText: {
      color: '#fff',
      fontSize: 18,
      fontWeight: '400',
    },
  });
  
  export default WeatherApp;