/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';

import { LineChart } from 'react-native-chart-kit'

import {
  Dimensions,
  FlatList,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  useColorScheme,
  View,
  Image
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import SearchableMultiSelect from '../Components/SearchableMultiSelect';

import { GetWeather } from '../Network/Api';

import MapView from 'react-native-maps';




const primaryDarkTextColor = '#03B6CA';
const primaryLightTextColor = '#00DCF4';




const Section = ({ children, title }) => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};
const WEATHER_ICON =
  'https://www.freeiconspng.com/uploads/weather-icon-png-25.png';

const SCREEN_WIDTH = Dimensions.get('screen').width;
const SCREEN_HEIGHT = Dimensions.get('screen').height;
const ASPECT_RATIO = SCREEN_WIDTH / SCREEN_HEIGHT;
const LATITUDE_DELTA = 10;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const cities = [
  { label: 'Rawalpindi', value: 'Rawalpindi' },
  { label: 'Lahore', value: 'Lahore' },
  { label: 'Karachi', value: 'Karachi' },
  { label: 'Islamabad', value: 'Islamabad' },
]
const chartConfig = {
  backgroundGradientFrom: '#fff',
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: '#fff',
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
  strokeWidth: 2, // optional, default 3
  barPercentage: 0.5,
  useShadowColorFromDataset: false, // optional
};
const celsiustemp = (k) => {
  let C = k - 273.15;
  return C.toFixed(2);
}

const App = () => {
  const [weatherData, setWeatherData] = useState(null)
  const [mapVisible, setMapVisible] = useState(false)
  const [city, setCity] = useState()
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    // backgroundColor: 'pink',
    height: SCREEN_HEIGHT
    // flex:1
  };



  const ViewCityOnMap = () => {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: city.lat,
            longitude: city.lon,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}
        >


        </MapView>
      </View>
    );
  };


  const renderWeatherCards = (item, index) => {
    item = item["item"]
    index = item["index"]
    return (
      <View style={styles.card}>
        <Image
          style={{ height: 50, width: 50, paddingTop: 10 }}
          resizeMethod="auto"
          resizeMode="center"
          source={{ uri: WEATHER_ICON }}
        />
        <Text
          style={{
            color: '#fff',
            fontSize: 12,
            letterSpacing: 1,
          }}>
          {celsiustemp(item?.main?.temp) ? celsiustemp(item?.main?.temp) : "Temp"} C
        </Text>
        <Text style={{ color: '#fff' }}>{item?.weather[0]?.description ? item?.weather[0]?.description : "Description"}</Text>
        <Text style={{ color: '#fff', paddingBottom: 10 }}>{item?.dt_txt ? item?.dt_txt : Date}</Text>
      </View>
    );
  };

  const graphData = {
    labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
    datasets: [
      {
        data: [20, 45, 28, 80, 99, 43],
        color: () => `${primaryDarkTextColor}`, // optional
        strokeWidth: 2, // optional
      },
    ],
    legend: ['Temprature'], // optional
  };
  const Heading = ["Weather Listing Heading",];


  const weatherUpdate = async (city) => {
    let data = await GetWeather(city);
    console.log(data["city"]["coord"])
    setWeatherData(data["list"])
    setCity(data["city"]["coord"])
  }
  return (
    <SafeAreaView style={backgroundStyle}>
      <Modal
        animationType="slide"
        visible={mapVisible}
        onRequestClose={() => setMapVisible(!mapVisible)}>
        <ViewCityOnMap />
      </Modal>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <Section title={Heading[0]} />

      <SearchableMultiSelect
        onChangeText={(city, visible) => weatherUpdate(city)}
        placeholder={'Select City'}
        includeSearch={false}
        data={cities}
        maxWidth='90%'
        arrayItem={"value"}

      />
      {/* <ScrollView style={{flex:1}}> */}
      <View style={styles.weatherMainView}>
        <FlatList
          horizontal
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
          legacyImplementation={false}
          style={{ flexGrow: 0 }}
          data={weatherData}
          renderItem={(item) => renderWeatherCards(item)}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={styles.weatherListContainerstyle}
          ListEmptyComponent={() => {
            return (
              <View style={styles.emptyView}>
                <Text style={styles.emptyList}>Please Select City</Text>
              </View>
            );
          }}
        />

        <ScrollView horizontal>
          <View style={{ marginHorizontal: 16, alignItems: 'center', flex: 1 }}>
            <LineChart
              style={{ backgroundColor: '#fff' }}
              data={graphData}
              width={SCREEN_WIDTH}
              height={190}
              chartConfig={chartConfig}
            />
          </View>
        </ScrollView>
        <TouchableOpacity style={styles.buttonContainer}
          onPress={() => setMapVisible(true)}
        >
          <Text style={styles.buttonText}>View City on Map</Text>
        </TouchableOpacity>
        {/* </ScrollView> */}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  buttonContainer: {
    height: 50,
    backgroundColor: primaryLightTextColor,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    borderRadius: 5,
    padding: 5,
    marginTop:15
  },
  buttonText: {
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  graphConfigStyle: {
    backgroundColor: '#e26a00',
    backgroundGradientFrom: '#fb8c00',
    backgroundGradientTo: '#ffa726',
    decimalPlaces: 2, // optional, defaults to 2dp
    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    style: {
      borderRadius: 16
    }
  },
  graphStyle: {
    marginVertical: 8,
    borderRadius: 16,
    alignSelf: 'center'

  },
  emptyList: { color: 'red' },
  emptyView: {
    justifyContent: 'center',
    width: SCREEN_WIDTH,
    alignItems: 'center'
  },
  weatherListContainerstyle: {
    height: 150, padding: 10, borderWidth: 1
  },
  weatherMainView: {
    marginTop: 15,
    marginHorizontal: 10
  },
  card: {
    borderRadius: 8,
    backgroundColor: primaryDarkTextColor,
    marginHorizontal: 5,
    paddingHorizontal: 2,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 10,
  },
  map: {
    height: SCREEN_HEIGHT,
    width: SCREEN_WIDTH
  }
});

export default App;
