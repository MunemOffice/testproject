/**
 * Test Drop Down 
 * searchable disabled 
 * modal added
 */

import React, { useState, useEffect } from 'react';
import {
  FlatList,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  Image,
  TextInput,
  Modal,
} from 'react-native';
// import { set } from 'react-native-reanimated';
import AntDesign from 'react-native-vector-icons/AntDesign';
// import {COLORS} from './ThemeFlatlist';

function SearchableMultiSelect(props) {
  const {
    data,
    // value = '',
    onChangeText,
    placeholder = 'Select',
    includeSearch = false,
    placeholderTextColor = '#000',
    itemtextStyle = {},
    backgroundStyle = {},
    maxWidth = '100%',
    search,
    arrayItem,
    marginLefts = false,
    onPress
  } = props;
  const selectedItems = [];
  const [value, setvalue] = useState('');
  const [show, setShow] = useState(false);
  const [icon, setIcon] = useState(null);
  const [items, setItems] = useState();
  const [zIndex, setZIndex] = useState(false);

  useEffect(() => {
    setItems(data);
    setvalue(placeholder)
    setShow(false)
  }, []);

  const RenderListItem = ({ item, index }) => {
    return search ? (
      <>
        <TouchableOpacity
          onPress={() => {
            onChangeText(item[search],false),
              setvalue(item[search])
            setIcon(item.icon),
              setShow(false),
              setZIndex(false);
          }}
          style={styles.TouchableOpacityStyle}
        >
          {item.icon && (
            <Image
              source={{ uri: item.icon }}
              style={{
                height: 40,
                width: 40,
                resizeMode: 'contain',
                borderRadius: 50,
              }}
            />
          )}
          <Text style={[styles.textStyle, itemtextStyle]}>{item[search]}</Text>
        </TouchableOpacity>
      </>
    )
      :
      (<>
        <TouchableOpacity
          onPress={() => {
            onChangeText(item[arrayItem],false),
              setvalue(item[arrayItem])
            setIcon(item.icon),
              setShow(false),
              setZIndex(false);
           
          }}
          style={styles.TouchableOpacityStyle}
        >
          {item.icon && (
            <Image
              source={{ uri: item.icon }}
              style={{
                height: 40,
                width: 40,
                resizeMode: 'contain',
                borderRadius: 50,
              }}
            />
          )}
          <Text style={[styles.textStyle, itemtextStyle]}>{item[arrayItem]}</Text>
        </TouchableOpacity>
      </>
      );
  };
  return (
    <>
      <View
        style={[styles.parentView, {  width: maxWidth, height: 40 }, backgroundStyle]}
      >
         <TouchableOpacity
            onPress={() => {
              setShow(!show)
              setZIndex(!zIndex)
              // onPress(show)
            }}
            style={styles.dropdownButtonStyle}>
        <View style={[styles.searchStyle, ]}>
          {icon && (
            <Image
              source={{ uri: icon }}
              style={{
                height: 30,
                width: 30,
                resizeMode: 'contain',
                borderRadius: 50,
                marginHorizontal: 4,
              }}
            />
          )}
          {search ? <TextInput
            placeholderTextColor={placeholderTextColor}
            editable={true}
            style={styles.input}
            onChangeText={val => {
              setvalue(val)
              onChangeText(val), setIcon(null),
                //  val ? setShow(true) : setShow(false)
                val ?
                  setItems(items.filter(e => {
                    return e[search].toLowerCase().match(val.toLowerCase())
                  }))
                  : setItems(data);
            }}
            value={value}
            placeholder={show ? 'Type here' : placeholder}
          /> :
            <Text
              placeholderTextColor={placeholderTextColor}
              style={styles.input}

              placeholder={show ? 'Type here' : placeholder}
            >{value}</Text>
          }
          
            <AntDesign name={ 'caretdown'} color="#000" />
      
        </View>
        </TouchableOpacity>
        <Modal
        visible={show}
        style={{backgroundColor:'black', borderRadius: 4,alignSelf:'center',}}>
          <FlatList
            data={
              items
            }
            renderItem={(item, index) => RenderListItem(item, index)}
            keyExtractor={(item, index) => index.toString()}
            ListEmptyComponent={() => {
              return (
                <View style={styles.emptyView}>
                  <Text style={{ color: 'red' }}>No Item Found</Text>
                </View>
              );
            }}
          />
        </Modal>
      </View>
      
      
    </>
  );
}
const styles = StyleSheet.create({
  parentView: {
    borderWidth: 1,
    padding: 8,
    borderRadius: 8,
    alignSelf:'center',
    backgroundColor:'white'
  },
  TouchableOpacityStyle: {
    flex: 1,
    marginVertical: 2,
    padding: 8,
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
    borderBottomWidth: 1,
    backgroundColor:'white',
  },
  textStyle: {
    flex: 1,
    fontWeight: '500',
    paddingHorizontal: 16,
    fontSize: 14,
    color: 'black'
  },
  emptyView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    fontSize: 15,
    color: '#000',
    flex: 1,
    textAlignVertical: 'center',
    backgroundColor: 'white'
  },
  searchStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 8,
    maxHeight: 60,
    color: 'black'


  },
  dropdownButtonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '80%',
  },
});

export default SearchableMultiSelect;
