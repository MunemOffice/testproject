/**
 * @format
 */

import React,{AppRegistry, LogBox} from 'react-native';
import {App} from './source/Main';
import Weather from './source/Screens/HomeScreen/Weather'
import {name as appName} from './app.json';

function Main(){
    LogBox.ignoreAllLogs(true);
    return App
}

AppRegistry.registerComponent(appName, ()=> Main());
